# README #

### What is this repository for? ###

This application is a Spring Boot server, with Spring MVC enabled, to do tests about how the order of the webjars dependencies in POM file affects in execution time. 

### How do I get set up? ###

Download the repository and execute "mvn clean package". After that, you can start the server executing the generated server artifact.